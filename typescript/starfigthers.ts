import {Spacecraft, Containership} from './base-chips'

export class MilleniumFalcon extends Spacecraft implements Containership {

    cargoContainers = 2
    
    constructor() {
        super('hyperdrive')
        this.cargoContainers = 4
    }

    jumpIntoHyperspace() {
        if(Math.random() >= 0.5) {
            super.jumpIntoHyperspace()
        } else {
            console.log('Failed to jump into hyperspace')
        }
    }
}